# 775 Assignment 1

The goal of this assignment is to implement one of the following data center topologies:
- K-ary Fat Tree
- BCube (k, n)

You can choose any programing language and write a program that takes k and/or n
parameters and creates the target topology in an output file. Notice that each team is
required to implement one of the above topologies. Every team needs to submit the
followings:

1) Compiled source code in pdf
2) An output file containing the generated topology in pdf
3) A demo of your code execution with different input values and simulation of your
   topology using Mininet (http://mininet.org/walkthrough/ ) in MP4

**We Choose K-ary Fat Tree.**

1. K-ary Fat Tree
   This program takes k as an input and generates the topology in an output file. You may use
   the following naming convention for nodes (including servers and switches) starting from
   the far left server ( “N0” ). For example, the following picture shows a 4-ary Fat Tree_based
   data center. Servers are assigned “N0” to “N15”, followed by Edge switches (“N16” to
   “N23”), Aggregation switches (“N24” to “N31”) and Core switches (“N32” to “N35”)
   respectively.

![1675547131142](image/README/1675547131142.png)

To generate the topology in output file, you need to show the connection of each pair
of nodes in the data center. If there is a direct connection between two nodes Ni and
Nj in your topology, you should write:
Ni Nj 1
If there is no direct connection between two nodes Ni and Nj in your topology, you
should write:
Ni Nj 9999
So your output file should be in the following format:  
N0 N1 9999  
N0 N2 9999  
.  
.  
N0 N16 1  
.  
.  
We can refer to this structure:
![img.png](image/README/img.png)