from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import RemoteController
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
 
class MyTopo(Topo):
 
    def __init__(self):
        super(MyTopo,self).__init__()
 
        k=4
        pod=k
        CoreSwitchNumber = (pod//2)**2
        AggregationSwitchNumber = pod*pod//2
        EdgeSwitchNumber = AggregationSwitchNumber
        ServerNumberInOneEdgeSwitch = k//2
        coreSwitch = []
        aggregateSwitch = []
        edgeSwitch = []
 
        # init core switch
        for i in range(CoreSwitchNumber):
            c_sw = self.addSwitch('c{}'.format(i+1))
            coreSwitch.append(c_sw)
 
        # init aggregate switch
        for i in range(AggregationSwitchNumber):
            a_sw = self.addSwitch('a{}'.format(CoreSwitchNumber+i+1))
            aggregateSwitch.append(a_sw)
 
         # init edge switch
        for i in range(EdgeSwitchNumber):
            e_sw = self.addSwitch('e{}'.format(CoreSwitchNumber+AggregationSwitchNumber+i+1))
            edgeSwitch.append(e_sw)

        # add link from core switch to aggreate switch
        for i in range(CoreSwitchNumber):
            c_sw=coreSwitch[i]
            start=i%(pod//2)
            for j in range(pod):
                self.addLink(c_sw,aggregateSwitch[start+j*(pod//2)])
 
        # add link from aggreate switch to edge switch
        for i in range(AggregationSwitchNumber):
            group=i//(pod//2)
            for j in range(pod//2):
                self.addLink(aggregateSwitch[i],edgeSwitch[group*(pod//2)+j])
        
        # add link from edge switch to host
        for i in range(EdgeSwitchNumber):
            for j in range(ServerNumberInOneEdgeSwitch):
                hs = self.addHost('h{}'.format(i*ServerNumberInOneEdgeSwitch+j+1))
                self.addLink(edgeSwitch[i],hs)
 
 
topos = {"mytopo":(lambda:MyTopo())}

