class Node:
    def __init__(self, index):
        self.index = index
        self.links = set()

    def link(self, target: 'Node'):
        self.links.add(target.index)
        target.links.add(self.index)

    def get_links(self, total):
        res = ""
        for i in range(total):
            if i != self.index:
                if i in self.links:
                    res += "N%d N%d %d\n" % (self.index, i, 1)
                else:
                    res += "N%d N%d %d\n" % (self.index, i, 9999)
        return res


def generate_topology(k):
    if k < 2 or k % 2 != 0:
        print("Invalid K")
        return
    # Overall number of switches: k * k + (k / 2) ^ 2 = (k ^ 2) * 5 / 4
    n_switches = pow(k, 2) * 5 // 4
    print("n_switches:", n_switches)
    # Overall number of core switches: (k / 2) * (k / 2) = (k / 2) ^ 2
    n_core = pow(k // 2, 2)
    print("n_core:", n_core)
    # aggregation and edge switches count
    n_aggr_and_edge = n_switches - n_core
    print("n_aggr_and_edge:", n_aggr_and_edge)
    # Overall number of servers: k * (k / 2) * (k / 2) = (k ^ 3) / 4
    n_servers = pow(k, 3) // 4
    print("n_servers:", n_servers)
    # total nodes
    n_total = n_switches + n_servers
    print("n_total:", n_total)
    # other numbers
    n_edge_switches_per_pod = int(k / 2)
    n_aggr_switches_per_pod = n_edge_switches_per_pod
    n_pods = k
    n_edge = n_pods * n_edge_switches_per_pod
    print("n_edge:", n_edge)
    n_aggr = n_edge
    print("n_aggr:", n_aggr)
    servers = []
    edges = []
    aggrs = []
    cores = []
    nodes = []
    index = 0
    # Generate server nodes
    for i in range(n_servers):
        node = Node(index)
        servers.append(node)
        nodes.append(node)
        index += 1
    # Generate edge switch nodes
    for i in range(n_edge):
        node = Node(index)
        edges.append(node)
        nodes.append(node)
        index += 1
    # Generate aggr switch nodes
    for i in range(n_aggr):
        node = Node(index)
        aggrs.append(node)
        nodes.append(node)
        index += 1
    # Generate core switch nodes
    for i in range(n_core):
        node = Node(index)
        cores.append(node)
        nodes.append(node)
        index += 1
    """Create links between servers and edge switches"""
    count = 0
    edge_switch_index = 0
    for i in range(n_servers):
        servers[i].link(edges[edge_switch_index])
        count += 1
        if count % n_edge_switches_per_pod == 0:
            edge_switch_index = edge_switch_index + 1
    """Create links between edge switches and aggr switches"""
    pod_edges = []
    pod_aggrs = []
    tmp = []
    count = 0
    # Get edge switches in pods
    for edge in edges:
        tmp.append(edge)
        count += 1
        if count % n_edge_switches_per_pod == 0:
            pod_edges.append(tmp)
            tmp = []
    tmp = []
    count = 0
    # Get aggr switches in pods
    for aggr in aggrs:
        tmp.append(aggr)
        count += 1
        if count % n_aggr_switches_per_pod == 0:
            pod_aggrs.append(tmp)
            tmp = []
    # Concat pod_aggrs and pod_edges
    pods = [x for x in map(lambda a, b: [a, b], pod_aggrs, pod_edges)]
    # Generate links for each pod
    for pod in pods:
        for edge in pod[1]:
            for aggr in pod[0]:
                edge.link(aggr)
    """Create links between aggr switches and core switches"""
    # Generate links
    core_switches_per_aggr_switch = n_core // n_aggr_switches_per_pod
    print("core_switches_per_aggr_switch:", core_switches_per_aggr_switch)
    # Enable the rotate-shift of the core links as 775 slides
    shift_index = 0
    for pod_aggr in pod_aggrs:
        core_index = 0
        pod_aggr_index = 0
        while core_index < n_core:
            pod_aggr[pod_aggr_index].link(cores[(core_index + shift_index) % n_core])
            core_index += 1
            if core_index % core_switches_per_aggr_switch == 0:
                pod_aggr_index += 1
        shift_index += 1

    # Write results
    res = ""
    for i in range(n_total):
        node = nodes[i]
        res += node.get_links(n_total)
    with open("topology.txt", "w+") as f:
        f.write(res)
        print("Topology saved successfully")


if __name__ == "__main__":
    generate_topology(4)
